# MatrixProcessor

## Basic Features

This is a Java Spring Boot application (Java 17) where it processes a matrix in the form of a csv file to get the inverted matrix, 
to flatten the matrix, to the get the sum and product of the matrix. 

Please see the comments in the src code for further details.

## Getting started

1. Checkout the project
2. Make sure all the dependencies are downloaded 
3. Upload your csv file in the '_filefolder_' under the resources folder.
4. Start running the application


## Sample CURL for Testing
```
curl --location --request POST 'http://localhost:1052/infinit-o-app-demo/processCsv?fileName=matrix.csv'
```

where the fileName is the name of the .csv file. Such file must be uploaded in the '_filefolder_' under the resources folder.

## Sample Response:
```
{
    "givenMatrix": [
        [1,2,3],
        [4,5,6],
        [7,8,9]
    ],
    "invertedMatrix": [
        [1,4,7],
        [2,5,8],
        [3,6,9]
    ],
    "flattenedMatrix": "1, 2, 3, 4, 5, 6, 7, 8, 9",
    "totalSum": 45,
    "totalProduct": 362880
}
```
