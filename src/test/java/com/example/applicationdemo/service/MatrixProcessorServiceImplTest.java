package com.example.applicationdemo.service;

import com.example.applicationdemo.exception.InvalidMatrixDimensionException;
import com.example.applicationdemo.model.ProcessedMatrix;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MatrixProcessorServiceImplTest {

    @InjectMocks
    private MatrixProcessorServiceImpl matrixProcessorService;


    @Test
    void whenProcessingMatrix_Success() {
        int[][] matrixTest = {
                {1,2,3},
                {0,9,8},
                {6,4,5}
        };

        int[][] invertedMatrixTest = {
                {1,0,6},
                {2,9,4},
                {3,8,5}
        };

        ProcessedMatrix expectedOutput = new ProcessedMatrix();
        expectedOutput.setGivenMatrix(matrixTest);
        expectedOutput.setInvertedMatrix(invertedMatrixTest);
        expectedOutput.setFlattenedMatrix("1, 2, 3, 0, 9, 8, 6, 4, 5");
        expectedOutput.setTotalSum(38);
        expectedOutput.setTotalProduct(0);

        ProcessedMatrix actualOutput = matrixProcessorService.processMatrix(matrixTest);

        Assertions.assertEquals(actualOutput, expectedOutput);
    }

    @Test
    void whenProcessingMatrix_invalidMatrixDimension_ThrowsInvalidMatrixDimensionException() {
        int[][] matrixTest = {
                {1,2,3},
                {0,9,8}
        };

        Assertions.assertThrows(InvalidMatrixDimensionException.class,
                () -> matrixProcessorService.processMatrix(matrixTest));
    }
}
