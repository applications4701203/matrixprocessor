package com.example.applicationdemo.model;

import lombok.Data;

@Data
public class ProcessedMatrix {

    private int[][] givenMatrix;
    private int[][] invertedMatrix;
    private String flattenedMatrix;
    private int totalSum;
    private int totalProduct;
}
