package com.example.applicationdemo.controller;

import com.example.applicationdemo.converter.CsvFileMatrixConverter;
import com.example.applicationdemo.exception.FileNonExistentException;
import com.example.applicationdemo.exception.InvalidMatrixDimensionException;
import com.example.applicationdemo.model.ProcessedMatrix;
import com.example.applicationdemo.service.MatrixProcessorService;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.constraints.Pattern;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/processCsv", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@RequiredArgsConstructor
public class MatrixProcessorController {

    private final MatrixProcessorService matrixProcessorService;
    private final CsvFileMatrixConverter csvFileMatrixConverter;

    /**
     * API endpoint for matrix processing to invert, flatten, get the sum and product of the elements of the matrix.
     *
     * @param fileName not null; must be a csv file containing the matrix
     * @throws FileNonExistentException if the file is not in the filefolder
     * @throws ConstraintViolationException if the filename does not end with .csv
     * @throws InvalidMatrixDimensionException if the matrix is not a square
     *
     * @return ProcessedMatrix
     */
    @PostMapping
    public ProcessedMatrix processCsv(@RequestParam("fileName")
                                      @Pattern(regexp = ".+\\.csv$" , message = "Invalid file name!") String fileName) {

        Resource resourceFile = new ClassPathResource("filefolder/" + fileName);
        return matrixProcessorService.processMatrix(csvFileMatrixConverter.convert(resourceFile));
    }
}
