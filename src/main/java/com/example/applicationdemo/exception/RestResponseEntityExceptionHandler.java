package com.example.applicationdemo.exception;

import com.example.applicationdemo.exception.model.ErrorResponse;
import com.example.applicationdemo.exception.model.RestError;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    /**
     * Exception handler to display ID, Details, and Message of the Exception.
     *
     */
    @ExceptionHandler(FileNonExistentException.class)
    public ResponseEntity<ErrorResponse> fileNonExistentExceptionHandler() {
        RestError re = new RestError();
        re.setId("FNE001");
        re.setDetails("FILE_NOT_EXISTENT");
        re.setMessage("File does not exist in filefolder.");
        ErrorResponse er = new ErrorResponse(re);

        return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> constraintViolationExceptionHandler() {
        RestError re = new RestError();
        re.setId("CVE001");
        re.setDetails("FILE_NAME_FORMAT_INVALID");
        re.setMessage("File name format is invalid.");
        ErrorResponse er = new ErrorResponse(re);

        return new ResponseEntity<>(er, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidMatrixDimensionException.class)
    public ResponseEntity<ErrorResponse> invalidMatrixDimensionExceptionHandler() {
        RestError re = new RestError();
        re.setId("IMD001");
        re.setDetails("MATRIX_DIMENSIONS_INVALID");
        re.setMessage("The rows and columns of the matrix are not equal.");
        ErrorResponse er = new ErrorResponse(re);

        return new ResponseEntity<>(er, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
