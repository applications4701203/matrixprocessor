package com.example.applicationdemo.service;

import com.example.applicationdemo.model.ProcessedMatrix;

public interface MatrixProcessorService {

    ProcessedMatrix processMatrix(int[][] matrix);

    int[][] invertMatrix(int[][] matrix);
    String flattenMatrix(int[][] matrix);
    int sumOfMatrixInt(int[][] matrix);
    int productOfMatrixint(int[][] matrix);
}
