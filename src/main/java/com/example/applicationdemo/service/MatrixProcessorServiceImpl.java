package com.example.applicationdemo.service;

import com.example.applicationdemo.exception.InvalidMatrixDimensionException;
import com.example.applicationdemo.model.ProcessedMatrix;
import org.springframework.stereotype.Service;

@Service
public class MatrixProcessorServiceImpl implements MatrixProcessorService{

    /**
     * Main service method to invert, flatten, get the sum and product of the matrix.
     * Other operations are in separate methods that can be called independently of each other.
     *
     * @param matrix in the form of int[][] - 2D int array.
     * @throws InvalidMatrixDimensionException if the matrix is not a square matrix.
     * @return ProcessedMatrix
     */
    @Override
    public ProcessedMatrix processMatrix(int[][] matrix) {

        validateMatrixSize(matrix);

        ProcessedMatrix pm = new ProcessedMatrix();

        pm.setGivenMatrix(matrix);
        printMatrix(matrix);

        pm.setInvertedMatrix(invertMatrix(matrix));
        pm.setFlattenedMatrix(flattenMatrix(matrix));
        pm.setTotalSum(sumOfMatrixInt(matrix));
        pm.setTotalProduct(productOfMatrixint(matrix));

        return pm;
    }

    @Override
    public int[][] invertMatrix(int[][] matrix) {

        int intRows = matrix.length;
        int intCols = matrix[0].length;

        int[][] invertedMatrix = new int[intCols][intRows];

        for (int i = 0 ; i < intRows ; i++) {
            for (int j = 0 ; j < intCols ; j++) {
                invertedMatrix[j][i] = matrix[i][j];
            }
        }

        System.out.println("Inverted Matrix: ");

        printMatrix(invertedMatrix);

        return invertedMatrix;
    }

    @Override
    public String flattenMatrix(int[][] matrix) {
        StringBuilder flattenedMatrix = new StringBuilder();

        System.out.println("Flattened Matrix: ");

        for (int i = 0 ; i < matrix.length ; i++) {
            for (int j = 0 ; j < matrix[i].length ; j++) {
                flattenedMatrix.append(matrix[i][j]);
                if (i != matrix.length - 1 || j != matrix[i].length - 1) flattenedMatrix.append(", ");
            }
        }

        System.out.println(flattenedMatrix);

        return flattenedMatrix.toString();
    }

    @Override
    public int sumOfMatrixInt(int[][] matrix) {
        int totalSum = 0;

        for (int[] ints : matrix) {
            for (int anInt : ints) {
                totalSum += anInt;
            }
        }

        System.out.println("Total Sum: " + totalSum);
        return totalSum;
    }

    @Override
    public int productOfMatrixint(int[][] matrix) {
        int totalProduct = 1;

        for (int[] ints : matrix) {
            for (int anInt : ints) {
                totalProduct *= anInt;
            }
        }

        System.out.println("Total Product: " + totalProduct);
        return totalProduct;
    }

    private void printMatrix(int[][] matrix) {
        for (int[] ints : matrix) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }

    private void validateMatrixSize(int[][] matrix) {

        if(matrix == null || matrix.length == 0) {
            throw new InvalidMatrixDimensionException();
        }

        if (matrix[0].length != matrix.length) {
            throw new InvalidMatrixDimensionException();
        }
    }
}
