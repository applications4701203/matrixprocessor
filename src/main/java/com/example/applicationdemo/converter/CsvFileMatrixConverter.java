package com.example.applicationdemo.converter;

import com.example.applicationdemo.exception.FileNonExistentException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CsvFileMatrixConverter implements Converter<Resource, int[][]> {

    /**
     * Method to convert the csv file into a matrix int[][]
     *
     * @param source from the processed csv file folder called by the Controller
     * @throws FileNonExistentException if file does not exist in the filefolder
     * @return int[][] the matrix in the form of a 2D array
     */
    @Override
    public int[][] convert(@NonNull Resource source) {

        List<int[]> matrixRows = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(source.getInputStream()))) {
            String fileLine;
            while ((fileLine = br.readLine()) != null) {
                String[] fileValues = fileLine.split(",");
                int[] fileRow = new int[fileValues.length];

                for (int i = 0 ; i < fileValues.length ; i++) {
                    fileRow[i] = Integer.parseInt(fileValues[i]);
                }

                matrixRows.add(fileRow);
            }
        } catch (IOException e) {
            throw new FileNonExistentException();
        }

        return matrixRows.toArray(new int[matrixRows.size()][]);
    }
}
